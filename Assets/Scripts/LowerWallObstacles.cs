﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerWallObstacles : MonoBehaviour {


	[SerializeField]
	GameObject Obstacle;
	[SerializeField]
	GameObject player;

	int a = 30;
	private int i=0,j=0,c;
	int[] d = new int [50];

	static float value = 5f;

	Vector3 Lastposition = new Vector3(2f,-0.4f,-0.3f);

	private List<GameObject> activeObstacles;

	void Start () 
	{
		activeObstacles = new List<GameObject> ();
	}
	void Update () {

		float PlayerXposition = player.transform.position.x;

		if (PlayerXposition > value) {
			spawnObstacles ();
			value = value + 10;
		}

		if (PlayerXposition > (d[j]+5)) {

			Destroy (activeObstacles [0]);
			activeObstacles.RemoveAt (0);
			j++;
		}
	}
	private void spawnObstacles()
	{
		float b= Random.Range(50f , 10f);
		GameObject _Object = Instantiate (Obstacle) as GameObject;
		_Object.transform.position = Lastposition + new Vector3 (b, 0,0);
		Lastposition = _Object.transform.position;
		d[i]= (int)Lastposition.x;
		//b[i] = a;
		i++;
		activeObstacles.Add (_Object);
	}
}
