﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
	public GameObject playerMovement;
	private Vector3 lastPlayerPosition;
	private float distanceToMove;

	void Start()
	{
		
		lastPlayerPosition = playerMovement.transform.position;
	}
	void Update()
	{
		distanceToMove = playerMovement.transform.position.x - lastPlayerPosition.x;
		transform.position = new Vector3 (transform.position.x + distanceToMove, transform.position.y, transform.position.z);
		lastPlayerPosition = playerMovement.transform.position;
	}

}
