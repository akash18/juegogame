﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goldenBoxGeneration : MonoBehaviour {

	[SerializeField]
	GameObject goldenBox;
	[SerializeField]
	GameObject player;

	static float value = 2f;

	Vector3 Lastposition = new Vector3(2f,1f,0.13f);

	private List<GameObject> activeObstacles;

	void Start () 
	{
		
	}
	void Update () {

		float PlayerXposition = player.transform.position.x;

		if (PlayerXposition > value) {
			spawnGoldBoxs ();
			value = value + 15;
		}

	}
	private void spawnGoldBoxs()
	{
		float xRandom= Random.Range(20f , 10f);
		//float yRandom = Random.Range (0f, 2.2f);

		GameObject _Gb = Instantiate (goldenBox) as GameObject;
		_Gb.transform.position = Lastposition + new Vector3 (xRandom, 0,0);
		Lastposition = _Gb.transform.position;
	}
}
