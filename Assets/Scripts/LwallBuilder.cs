﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LwallBuilder : MonoBehaviour {

	private float size;
	[SerializeField]
	GameObject Wall1;
	//[SerializeField]
	public GameObject player1;
	int a =30;
	static float value1 = 1f;
	Vector3 Lastposition1;
	private DestroyBox des;

	private List<GameObject> activewall1;


	void Start () 
	{
		size = Wall1.transform.localScale.x;
		Lastposition1= Wall1.transform.position;
		activewall1 = new List<GameObject> ();
		des = GameObject.Find ("Score").GetComponent<DestroyBox> ();
	}
	void Update () {

		float PlayerXposition1 = player1.transform.position.x;
		Debug.Log (PlayerXposition1);


		if (PlayerXposition1 > value1) {
			spawnWall ();
			value1 = value1 + 2;

		}


		if (PlayerXposition1 > a) {
			
			des.Score ();
				Destroy (activewall1 [0]);
				activewall1.RemoveAt (0);
				a = a + 10;
		}
	}
	private void spawnWall()
	{
		GameObject platform1 = Instantiate (Wall1) as GameObject;
		platform1.transform.position = Lastposition1 + new Vector3 (size, 0, 0);
		Lastposition1 = platform1.transform.position;
		activewall1.Add (platform1);
	}
}
