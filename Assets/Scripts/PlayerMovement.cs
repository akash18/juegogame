﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float moveSpeed;
	public float jumpForce;
	public Rigidbody myBox;



	void Update(){
		myBox.velocity = new Vector3 (moveSpeed, myBox.velocity.y, myBox.velocity.z);
		//Debug.Log (transform.position);
		if (Input.GetKey (KeyCode.Space)) {
			myBox.velocity = new Vector3 (myBox.velocity.x, jumpForce, myBox.velocity.z);

		}
	}

}
