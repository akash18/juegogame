﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DestroyBox : MonoBehaviour {

	public static int score = 0;
	public Text myText;

	void Start()
	{
		myText = GetComponent<Text> ();
	}
	void Update()
	{
		myText.text = score.ToString ();
	}
	public void  Score()
	{
		score += 2;
	}

	public void OnTriggerEnter(Collider col)
	{
		score += 5;
		Destroy (this.gameObject);

	}
}


